# Filter-Station expert interface

This is an expert interface Web-Application for n_ToF Filter-Stations

This application is developped according to our [guidelines](https://mro-dev.web.cern.ch/docs/std/en-smm-apc-web-guidelines.html).

# Documentation

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
cd deploy

# Add correct passwords in ConfigMap, replacing xxx
# Do never commit passwords !
vim beta.yaml

oc apply -f beta.yaml
```
