// @ts-check

/* must be done early */
import debug from 'debug';

// @ts-ignore
debug.useColors = () => false;

import chai from 'chai';
import { before } from 'mocha';
import server from 'karma-server-side';
import dirtyChai from 'dirty-chai';

/* force include router for coverage */
import '../src/router';
import './karma_init';

chai.use(dirtyChai);

before(function() {
  return server.run(function() {
    // add a guard to not add one event listener per test
    if (!this.karmaInit) {
      // $FlowIgnore
      this.karmaInit = true;
      /* do not accept unhandledRejection */
      process.on('unhandledRejection', function(reason) {
        console.log('UnhandledRejection:', reason);
        throw reason;
      });
    }

    return process.env['DEBUG'];
  })
  .then((/** @type {string} */ dbg) => {
    if (dbg) {
      localStorage['debug'] = dbg;
    }
  });
});
