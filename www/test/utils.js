// @ts-check

import { createLocalVue as tuCreateLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { defaultTo, delay, get, has, toString, wrap } from 'lodash';
import { makeDeferred } from '@cern/prom';

import BaseVue from "@cern/base-vue";
import appRouter from '../src/router';
import Vuex from 'vuex';

/**
 * @param  {Tests.Wrapper|Vue.Component} wrapper
 * @param  {() => boolean|any} cb
 * @param  {?number=}   timeout
 * @return {Promise<any>}
 */
export function waitFor(wrapper, cb, timeout) {
  // @ts-ignore
  var vm /** @type {Vue.Component} */ = (has(wrapper, 'vm')) ? wrapper.vm : wrapper;

  var def = makeDeferred();
  var resolved = false;
  var err = new Error('timeout'); /* create this early to have stacktrace */
  var timer = delay(def.reject.bind(def, err),
    defaultTo(timeout, 1000));

  function next() {
    /* $FlowIgnore */
    vm.$nextTick(() => {
      if (resolved) { return; }
      try {
        var ret = cb(); /* eslint-disable-line callback-return */
        if (ret) {
          clearTimeout(timer);
          resolved = true;
          def.resolve(ret);
        }
        else {
          delay(next, 200);
        }
      }
      catch (e) {
        clearTimeout(timer);
        resolved = true;
        def.reject(has(e, 'message') ? e : new Error(e));
      }
    });
  }

  delay(next, 200);
  return def.promise;
}

/**
 * @template T=Tests.Wrapper
 * @param  {Tests.Wrapper|Vue.Component} vm
 * @param  {() => T} test
 * @param  {?number=} timeout
 * @return {Promise<T>}
 */
export function waitForWrapper(vm, test, timeout) {
  return waitFor(vm, () => {
    const wrapper = test();
    // @ts-ignore
    return wrapper.exists() ? wrapper : false;
  }, timeout);
}

/**
 * @template T=any
 * @param  {Tests.Wrapper|Vue.Component} vm
 * @param  {() => T} test
 * @param  {any} value
 * @param  {?number=} timeout
 * @return {Promise<T>}
 */
export function waitForValue(vm, test, value, timeout) {
  /** @type {any} */
  var _val;
  return waitFor(vm, () => {
    _val = test();
    return _val === value;
  }, timeout)
  .catch((err) => {
    const msg = "Invalid result value: " + toString(_val) + " != " + value;
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

/**
 * @param  {HTMLElement|Vue.Component}  el
 * @return {boolean}
 */
export function isVisible(el) {
  /** @type {HTMLElement|null} */
  var elt;
  if (has(el, 'element')) {
    elt = get(el, 'element'); /* vue wrapper */
  }
  else if (has(el, '$el')) {
    elt = get(el, '$el'); /* vue component */
  }
  else {
    // @ts-ignore
    elt = el;
  }
  while (elt) {
    if (elt.hidden ||
      (elt.style &&
       (elt.style.visibility === 'hidden' ||
        elt.style.display === 'none'))) {
      return false;
    }
    elt = get(elt, 'parentElement');
  }
  return true;
}

export const TransitionStub = {
  template: `<div :is="tag"><slot></slot></div>`,
  props: { tag: { type: String, default: 'div' } }
};

export const stubs = {
  'transition-group': TransitionStub,
  'transition': TransitionStub
};

/**
 * @param  {any} options
 */
export function createLocalVue(options) {
  const local = tuCreateLocalVue();
  local.use(VueRouter);
  local.use(BaseVue, options);
  local.use(Vuex);
  return local;
}

/**
 * @param  {any?=} route
 */
export function createRouter(route) {
  const router = new VueRouter(appRouter.options);
  if (route) {
    router.push(route);
  }

  /* fix issue with vue-test-utils 31, router is not updated for some reasons */
  // @ts-ignore
  router.push = wrap(router.push, function(p, /** @type {RawLocation} */ arg) {
    router.app.$nextTick(() => p.call(router, arg));
  });
  return router;
}
