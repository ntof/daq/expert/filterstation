// @ts-check

import './karma_index';
import { createLocalVue, waitForValue } from './utils';
import { butils } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from 'karma-server-side';

import App from '../src/App.vue';
import appRouter from '../src/router';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Router', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      butils.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can mount website', async function() {
    wrapper = mount(App, {
      router: appRouter, localVue: createLocalVue({ auth: true }) });

    await waitForValue(wrapper,
      () => wrapper.findComponent({ name: 'FilterStation' }).exists(), true);
  });
});
