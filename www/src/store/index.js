// @ts-check

import { assign, get, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';

Vue.use(Vuex);

/** @type {V.Store<AppStore.State>} */
merge(storeOptions, {
  state: {
    dns: null,
    device: null
  },
  mutations: {
    queryChange(state, query) {
      state.dns = get(query, 'dns', null);
      state.device = get(query, 'device', null);
    },
    clear(state) {
      assign(state, { dns: null, device: null });
    }
  }
  /* NOTE: declare your store and modules here */
});
export default createStore();

export const sources = merge(baseSources, {
  /* NOTE: declare your global data-sources here */
});
