
export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    user: { username: string, [key: string]: any } | null
    dns: string | null
    device: string | null
  }

  interface UiState {
    showKeyHints: boolean
  }
}
