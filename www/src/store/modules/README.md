
# Vuex Store Modules

Create your modules here.

Import and register it in parent index.js file.

Documentation on vuex modules is available here: https://vuex.vuejs.org/guide/modules.html
