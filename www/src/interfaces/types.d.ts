// @ts-check
import { XmlData } from "redim-client";

/**
 * XmlData
 */
declare namespace XML {
  interface ParamDesc {
    index?: number,
    type: XmlData.DataType
    map?: ParamMap
    field?: string
  }

  export interface ParamMap {
    [key: string]: ParamDesc
  }
}
