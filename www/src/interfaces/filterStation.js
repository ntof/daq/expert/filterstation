// @ts-check
import { ParamType } from "./index";

/**
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/** @enum {number} */
export const FilterPosition = {
  UNKNOWN: 0,
  IN: 1,
  OUT: 2,
  MOVING: 3,
  IN_INTERLOCKED: 4,
  OUT_INTERLOCKED: 5,
  MOVING_INTERLOCKED: 6
};

/** @type ParamMap */
export const FilterStationParamsMap = {
  airPressure: { index: 1, type: ParamType.FLOAT },
  filter1: { index: 2, type: ParamType.ENUM },
  filter2: { index: 3, type: ParamType.ENUM },
  filter3: { index: 4, type: ParamType.ENUM },
  filter4: { index: 5, type: ParamType.ENUM },
  filter5: { index: 6, type: ParamType.ENUM },
  filter6: { index: 7, type: ParamType.ENUM },
  filter7: { index: 8, type: ParamType.ENUM },
  filter8: { index: 9, type: ParamType.ENUM }
};
