// @ts-check

import { find, get, keyBy, noop, omit } from 'lodash';

import { AreaList, TITLE, VERSION } from './Consts';
import { UrlUtilitiesMixin } from './utilities';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {{
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const options = {
  areaMap: keyBy(AreaList, 'dns')
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'App',
  mixins: [
    UrlUtilitiesMixin
  ],
  ...options,
  /**
   * @return {{ TITLE: string, VERSION: string }}
   */
  data() { return { TITLE, VERSION }; },
  computed: {
    ...mapState([ 'dns' ]),
    /** @return {string} */
    earName() {
      return get(find(AreaList, { dns: this.dns }), 'name', this.dns);
    }
  },

  methods: {
    /**
     * @this {Instance}
     * @param  {string} dns
     */
    async selectDns(dns) {
      var query = omit(this.$route.query, [ 'dns', 'device' ]);
      query.dns = dns;
      this.$router.push({ query }).catch(noop);
    }
  }
});

export default component;
