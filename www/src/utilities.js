// @ts-check

import Vue from 'vue';

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @param  {string} dns DIM dns hostname
 * @return {string} dns proxy url
 */
export function dnsUrl(dns/*: any */) {
  return _currentUrl + '/' + dns + '/dns/query';
}

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return 'a-' + (++id);
}


export const UrlUtilitiesMixin = Vue.extend({
  methods: {
    getProxy() {
      return _currentUrl;
    },
    currentUrl() {
      return _currentUrl;
    },
    /**
     * @param  {string} dns
     */
    dnsUrl(dns/*: any */) {
      return _currentUrl + '/' + dns + '/dns/query';
    }
  }
});
