// @ts-check

import { forEach, get, invert, isEmpty, size } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import { DicXmlParams } from 'redim-client';

import Vue from 'vue';
import { mapState } from 'vuex';

import { UrlUtilitiesMixin } from '../utilities';
import { FilterPosition } from '../interfaces/filterStation';
import { ParamType } from '../interfaces';

/**
 * @typedef {{
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const options = {
  filterPosMap: FilterPosition,
  filterPosName: invert(FilterPosition),
  /** @type {DicXmlParams|null} */
  client: null
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'FilterStationController',
  ...options,
  mixins: [
    UrlUtilitiesMixin
  ],
  /** @return {{ filters: number[], airPressure: number|null }} */
  data() {
    return { filters: [], airPressure: null };
  },
  computed: {
    ...mapState([ 'dns', 'device' ])
  },
  /** @this {Instance} */
  mounted() {
    this.$options.client = new DicXmlParams(this.device,
      { proxy: this.getProxy() }, this.dnsUrl(this.dns));
    this.$options.client.on('value', this.onData);
    this.$options.client.on('error', this.onError);
  },
  beforeDestroy() {
    if (this.$options.client) {
      this.$options.client.removeAllListeners();
      this.$options.client.close();
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} state
     */
    stateName(state /*: number */) {
      return get(this.$options.filterPosName, state);
    },
    /**
     * @this {Instance}
     * @param {number} state
     */
    stateClass(state /*: number */) {
      if (state === FilterPosition.IN || state === FilterPosition.IN_INTERLOCKED) {
        return [ "badge-primary" ];
      }
      else {
        return [ "badge-secondary" ];
      }
    },
    /**
     * @param  {any} data
     */
    onData(data) {
      this.airPressure = get(data, [ 1, 'value' ]);
      const filters = [];
      for (let i = 2; i < size(data); ++i) {
        filters.push(get(data, [ i, 'value' ]));
      }
      this.filters = filters;
    },
    /**
     * @param  {any} error
     */
    onError(error) {
      logger.error(error);
    },
    async doSubmit() {
      try {
        /** @type {any[]} */
        const params = [];
        forEach(this.$refs.input, (input, idx) => {
          const value = get(input, [ 'editValue' ]);
          const current = get(this.filters, [ idx ]);
          if (value && (current !== FilterPosition.IN)) {
            params.push({ index: idx + 2, type: ParamType.ENUM, value: FilterPosition.IN });
          }
          else if (!value && (current !== FilterPosition.OUT)) {
            params.push({ index: idx + 2, type: ParamType.ENUM, value: FilterPosition.OUT });
          }
        });
        if (!isEmpty(params) && this.$options.client) {
          await this.$options.client.setParams(params);
        }
      }
      catch (e) {
        logger.error(e);
      }
    }
  }

});
export default component;
