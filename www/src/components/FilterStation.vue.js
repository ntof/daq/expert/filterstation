// @ts-check

import { assign, find, get, noop } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import { DicXmlDataSet } from 'redim-client';

import Vue from 'vue';
import { mapState } from 'vuex';

import { UrlUtilitiesMixin } from '../utilities';
import FilterStationController from './FilterStationController.vue';

/**
 * @typedef {{
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */


const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'FilterStation',
  components: { FilterStationController },
  mixins: [
    UrlUtilitiesMixin
  ],
  /** @return {{ searching: boolean }} */
  data() {
    return { searching: false };
  },
  computed: {
    ...mapState([ 'dns', 'device' ])
  },
  watch: {
    dns: {
      immediate: true,
      handler() { this.findDevice(); }
    }
  },
  methods: {
    /**
     * @this {Instance}
     */
    async findDevice() {
      if (!this.dns || this.device) { return; }
      this.searching = true;
      try {
        const station = await DicXmlDataSet.get('EACS/Info',
          { proxy: this.getProxy() }, this.dnsUrl(this.dns))
        .then((/** @type {string} */ data) => DicXmlDataSet.parse(data))
        .then((/** @type {any} */ dataset) => {
          const ret = find(dataset, (d) => d.name === 'filterStation');
          return get(ret, [ 'value' ]);
        });
        var query = assign({}, this.$route.query, { device: station });
        this.$router.push({ query }).catch(noop);
      }
      catch (e) {
        logger.warning(e);
      }
      this.searching = false;
    }
  }

});
export default component;
