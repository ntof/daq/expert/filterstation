
# Components

Place your app-secific components here.

Don't hesitate to create sub-directories per page/topic in your application.

Components are split in two files:
- **\*.vue** : the template and style part of the component
- **\*.vue.js** : the Javascript code associated to the template

**Note:** this split eases Javascript files linting and IDE integration.
