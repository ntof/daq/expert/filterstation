// @ts-check
import Vue from 'vue';
import Router from 'vue-router';

import FilterStation from './components/FilterStation.vue';

import { default as store } from './store';

Vue.use(Router);

const router = new Router({
  routes: [
    { name: 'Home', path: '/', component: FilterStation },

    { path: '/index.html', redirect: '/' },
    { path: '/Layout', redirect: '/' }
  ]
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.afterEach((to, from) => { /* jshint unused:false */
  store.commit('queryChange', to.query);
});

export default router;
