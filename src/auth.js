// @ts-check
const
  { defaultTo, get, includes, isString, some } = require('lodash'),
  debug = require('debug')('auth'),
  passport = require('passport'),
  cookieSession = require('cookie-session'),
  { Issuer, Strategy } = require('openid-client');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 * @typedef {import('express').IRouter} IRouter
 */

/* to serialize info in session */
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

/**
 * @param {string|string[]} role required role or list of roles (one of)
 * @param {Request} req incoming request
 * @param {Response} res outgoing reply
 * @param {NextFunction} next routing next callback
 */
function roleCheck(role, req, res, next) {
  const isStringRole = isString(role);
  const userRoles = get(req.user, 'cern_roles');

  if (!req.user) {
    debug('unauth user -> /auth');
    return res.redirect(defaultTo(req.baseUrl, '') + '/auth');
  }
  else if (isStringRole && includes(userRoles, role)) {
    return next();
  }
  else if (!isStringRole && some(role, (r) => includes(userRoles, r))) {
    return next();
  }
  else {
    debug('permission denied');
    return res.render('denied', { baseUrl: req.baseUrl, role });
  }
}

module.exports = {
  /**
   *
   * @param {IRouter} app
   * @param {AppServer.Config} config
   * @param {(user: any) => any} [loginCallback]
   */
  async register(app, config, loginCallback) {
    var basePath = get(config, 'basePath', '');

    const oidcIssuer =
      await Issuer.discover('https://auth.cern.ch/auth/realms/cern');
    const client = new oidcIssuer.Client({
      client_id: get(config, 'auth.clientID'), /* eslint-disable-line camelcase */
      client_secret: get(config, 'auth.clientSecret'), /* eslint-disable-line camelcase */
      redirect_uris: [ get(config, 'auth.callbackURL') ], /* eslint-disable-line camelcase */
      post_logout_redirect_uris: [ get(config, 'auth.logoutURL') ] /* eslint-disable-line camelcase */
    });

    passport.use('oidc', new Strategy({ client },
      /** @type {import('openid-client').StrategyVerifyCallbackUserInfo<any>} */
      function(tokenSet, userInfo, done) {
        debug('user authenticated: %s', get(userInfo, 'email'));
        const user = tokenSet.claims();
        done(null, user);
        if (loginCallback) {
          loginCallback(user);
        }
      }));

    app.use(cookieSession({
      name: 'oidc:auth.cern.ch',
      secret: get(config, 'auth.clientSecret', 'default'),
      path: get(config, 'basePath') || '/',
      signed: true,
      overwrite: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.get('/auth', passport.authenticate('oidc'));
    app.get('/auth/callback', passport.authenticate('oidc', {
      successRedirect: basePath + '/',
      failureRedirect: basePath + '/auth'
    }));
    app.get('/auth/logout', function(req, res) {
      req.session = null;
      res.redirect(client.endSessionUrl());
    });
    app.get('/auth/me', function(req, res) { res.send(req.user); });
  },
  roleCheck: roleCheck
};
