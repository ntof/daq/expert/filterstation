// @ts-check
console.warn('Loading stub configuration');

module.exports = {
  title: 'FilterStation expert interface',
  basePath: '',
  beta: true,
  auth: {
    clientID: "base-website-template",
    clientSecret: "fce99307-a575-4fad-bff3-57214e569351",
    callbackURL: "http://localhost:8080/auth/callback",
    logoutURL: "http://localhost:8080"
  }
};
