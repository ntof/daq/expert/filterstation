const { defaultTo } = require('lodash');

/** @type {any} */
var config;
try {
  /* @ts-ignore */ /* eslint-disable-next-line global-require */
  config = /** @type {any} */ (require('/etc/app/config'));
}
catch (e) {
  /* eslint-disable-next-line global-require */
  config = /** @type {any} */ (require('./config-stub'));
}

config.port = defaultTo(config.port, 8080);
config.basePath = defaultTo(config.basePath, '');

module.exports = /** @type {AppServer.Config} */ (config);
